#!/usr/bin/env bash
set -e

###################
## CONFIGURATION ##
###################

# The cross compiler the script will download and use.
CROSSCOMPILER_URL="https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu.tar.xz"
CROSSCOMPILER_NAME="gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu"
UBUNTU_ISO_URL="https://cdimage.ubuntu.com/ubuntu/releases/20.04/release/ubuntu-20.04.4-preinstalled-server-arm64+raspi.img.xz"

###################

# Warn the user that the script will require ~6GB of disk space, and will install packages.
echo "WARN: This script requires >=6GB of free disk space to run, and will install packages required for building QEMU and fwupd. You should also run this script with superuser permissions. Continue? (Y/N)"
read consent
if [[ $consent != "Y" && $consent != "y" ]]; then
    exit 1
fi

# Check for sudo.
prompt=$(sudo -nv 2>&1)
if [ $? -eq 0 ]; then
    # Exit code of sudo-command is 0
    echo "Sudo status detected successfully."
elif echo $prompt | grep -q '^sudo:'; then
    echo "Has sudo, password input required."
else
    echo "You must run this script with superuser priveleges."
    exit 1
fi

# Uh oh. This is for testing only, root CA certs should be fine in real world.
export GIT_SSL_NO_VERIFY=1

# Install required dependencies.
sudo apt --yes install expect python3-pycryptodome python3-pyelftools gcc-arm-linux-gnueabihf libssl-dev

# Check that "repo" is installed.
if ! [ -x "$(command -v repo)" ]; then
  echo "ERROR: The 'repo' prerequisite dependency is not installed. Please install 'repo' before running this script." >&2
  exit 1
fi

# If the repo folder already exists, don't attempt to re-clone.
if [ ! -d "uboot-builder" ]; then
    # Create a subfolder to clone the repo into.
    mkdir -p uboot-builder
    cd uboot-builder

    # Clone the prototype repo here, then sync.
    repo init -u https://git.linaro.org/people/jose.marinho/u-boot-manifest.git -m fwu_BET0.xml
    repo sync
    cd ..
fi

# Grab tarball for the correct Arm cross compiler, unzip and set flag.
if [ ! -f compiler.tar.xz ]; then
    echo "Downloading cross compiler..."
    wget $CROSSCOMPILER_URL -O compiler.tar.xz
    tar -xf compiler.tar.xz
fi
cross_compile_absolute=$(realpath ./$CROSSCOMPILER_NAME/bin/aarch64-none-linux-gnu-gcc)
export CROSS_COMPILE=${cross_compile_absolute:0:$((${#cross_compile_absolute}-3))}
export CROSS_COMPILE64=${cross_compile_absolute:0:$((${#cross_compile_absolute}-3))}
echo "Downloaded cross compiler, set CROSS_COMPILE to $CROSS_COMPILE."
 
# Clone and build QEMU, point QEMU_BIN variable to our new built version.
if [ ! -d "qemu" ]; then
    git clone https://github.com/qemu/qemu
    cd qemu
    git checkout tags/v6.1.0
    mkdir -p build
    cd build
    ../configure --target-list=aarch64-softmmu
    make
else
    cd qemu/build
fi
export QEMU_BIN=$(realpath qemu-system-aarch64)
cd ../../

# Build remaining uboot-builder components.
cd uboot-builder
make qemu_arm64_defconfig
make addcapsuleconfig
make
cd ..

# Clone and build buildroot, set up onto a loop & mount.
if [ ! -d "buildroot" ]; then
    git clone https://github.com/buildroot/buildroot
fi
cd buildroot/
make aarch64_efi_defconfig
make
buildroot_loop=$(sudo losetup -f -o 32768 ./output/images/disk.img --show)
cd ..
mkdir -p buildroot_mnt
sudo mount $buildroot_loop buildroot_mnt/

# Generate the DTB file via. QEMU, copy into buildroot mount.
cd uboot-builder/
$QEMU_BIN -machine virt,dumpdtb=qemu-arm64.dtb,secure=on -cpu cortex-a57 -smp 2 -m 2048 -d unimp -monitor null -no-acpi -nographic -serial stdio   -serial tcp::5000,server,nowait  -drive if=virtio,format=raw,file=../buildroot/output/images/disk.img -drive if=pflash,unit=0,file=nor_flash.bin
sudo cp qemu-arm64.dtb ../buildroot_mnt/qemu-arm64.dtb
cd ..

# Download preinstalled Ubuntu image XZ and mount, set shadow from config.
if [ ! -f ubuntu.img ]; then
    wget $UBUNTU_ISO_URL -O ubuntu.img.xz
    unxz ubuntu.img.xz
fi
ubuntu_loop=$(sudo losetup -Pf ubuntu.img --show)
echo "Mounted Ubuntu image at $ubuntu_loop."
mkdir -p ubuntu_mnt
sudo mount $ubuntu_loop"p2" ubuntu_mnt/
sudo cp config/shadow ubuntu_mnt/etc/shadow

# Create the new final disk image.
sudo rm -f disk.img
touch disk.img
truncate disk.img --size 6G
disk_loop=$(sudo losetup -Pf disk.img --show)
echo "Mounted final virtual disk at $disk_loop."

# Create the GPT table, partitions on the fresh disk w/ parted.
# Refresh the loop to add new partitions.
sudo parted $disk_loop mklabel gpt mkpart primary fat32 4MiB 516MiB mkpart primary ext4 516MiB 6143MiB
sudo losetup -d $disk_loop
disk_loop=$(sudo losetup -Pf disk.img --show)
if [ ! -e $disk_loop"p1" ]; then
    echo "Partition loop $disk_loop partition 1 does not exist."
    exit 1
fi

# Create new partitions, set flags.
sudo mkfs.fat -F32 -v -I $disk_loop"p1"
sudo mkfs.ext4 -F -O ^64bit $disk_loop"p2"
sudo parted $disk_loop set 1 boot on
sudo parted $disk_loop set 1 esp on

# Mount these new partitions on disk.
mkdir -p disk_esp_mnt
mkdir -p disk_root_mnt
sudo mount $disk_loop"p1" disk_esp_mnt/
sudo mount $disk_loop"p2" disk_root_mnt/

# Copy everything into the final disk.
sudo cp -r ubuntu_mnt/* disk_root_mnt/
sudo cp -r buildroot_mnt/* disk_esp_mnt/
sudo cp ./config/system-local.conf disk_root_mnt/etc/dbus-1/
sudo mkdir -p disk_root_mnt/etc/fwupd/remotes.d
sudo cp ./config/arm.conf disk_root_mnt/etc/fwupd/remotes.d/
ls disk_root_mnt/
ls disk_esp_mnt/

# Cleanup.
sudo umount ubuntu_mnt
sudo umount buildroot_mnt
sudo umount disk_esp_mnt
sudo umount disk_root_mnt
sudo losetup -d $disk_loop
sudo losetup -d $buildroot_loop
sudo losetup -d $ubuntu_loop
sudo rm -r ubuntu_mnt
sudo rm -r buildroot_mnt
sudo rm -r disk_esp_mnt
sudo rm -r disk_root_mnt
echo "Copied contents onto final disk image, cleaned up."

# Set this disk as the virtual disk for the QEMU VM, start up the VM build process.
export VIRTDISK=$(realpath disk.img)
cp build-fwupd.sh uboot-builder/
cd uboot-builder/

sudo chmod +x build-fwupd.sh
./build-fwupd.sh
echo "Build complete."