#!/usr/bin/env bash

# Inform the user they should run "build.sh" first.
echo "WARN: This script assumes you have first run 'build.sh' to build your Ubuntu image, and utilises docker."
echo "In addition, you should have no services running on tcp/5432 (postgres) or tcp/6011 (LVFS) prior to running. (Y/N)"
read consent
if [[ $consent != "Y" && $consent != "y" ]]; then
    exit 1
fi

# Check for sudo.
prompt=$(sudo -nv 2>&1)
if [ $? -eq 0 ]; then
    # exit code of sudo-command is 0
    echo "Sudo status detected successfully."
elif echo $prompt | grep -q '^sudo:'; then
    echo "Has sudo, password input required."
else
    echo "You must run this script with superuser priveleges."
    exit 1
fi

# Install docker and docker-compose.
sudo apt --yes install docker-compose

# Clone LVFS website repository if it doesn't exist.
if [ ! -d "lvfs-website" ]; then
    echo "LVFS not already present, beginning build..."
    git clone https://gitlab.com/fwupd/lvfs-website.git

    # Create the database for the LVFS within docker.
    export DOCKER_BUILDKIT=1
    sudo chmod +x ./build-db.sh
    sudo docker-compose up -d postgres
    sudo docker-compose run --entrypoint="/app/build-db.sh" lvfs
fi
cd lvfs-website

# Create the download/upload/shard directories for the LVFS, if they don't exist.
if [ ! -d "mnt_uploads" ]; then
    mkdir mnt_uploads
fi
if [ ! -d "mnt_shards" ]; then
    mkdir mnt_shards
fi
if [ ! -d "mnt_downloads" ]; then
    mkdir mnt_downloads
fi

# Bring up the docker.
sudo docker-compose up