# fwupd Prototype Builder
This repository contains a collection of scripts allowing for the automated building and setup of:
- An Arm64 virtual machine running a locally compiled `fwupd` daemon.
- A locally running LVFS instance via. Docker, connected to the Arm64 virtual machine.

## Prerequisites
Before running these scripts, you must first have the following packages installed:
- `docker-compose`
- `expect`
- `xz-utils`
- `parted`
- `repo` ([install instructions](https://gerrit.googlesource.com/git-repo/+/refs/heads/master/README.md))

Any local services running on 6011/tcp, 5432/tcp (`postgres`), 6379/tcp (`redis`) and 6379/tcp (`celery`) must be terminated.
In addition to this, all scripts must have execute permission (`chmod +x *.sh`).

## Building
To build the virtual machine and LVFS sequentially, run the following:
```
./build.sh
./run-lvfs.sh
```

Once this is done, you can then start the virtual machine in another terminal window by performing:
```
./run-vm.sh
```

## Testing
Once both the VM and the LVFS are running, you can begin to upload firmware for testing onto the LVFS. To do this, navigate to the local LVFS instance at `localhost:6011`, and log in with the email "`sign-test@fwupd.org`" and password "`Pa$$w0rd`". Once this is done, navigate to `localhost:6011/lvfs/settings/create` to initialize the settings.

Now that you are logged in as the LVFS admin user and settings are configured, you can then create an organisation, device, and subsequently upload firmware to be downloaded and installed by the virtual machine. To access the locally built `fwupdtool` on the virtual machine to check the device's GUID and download firmware, log in as the root user (username and password "`root`"), and run the built binary located at `~/fwupd/build/src/fwupdtool`.