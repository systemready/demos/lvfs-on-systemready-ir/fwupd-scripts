#!/usr/bin/env bash

# Initialize python environment.
pip install virtualenv
python3 -m virtualenv env
source env/bin/activate
pip3 install -r /app/requirements.txt

# Initialize database using Flask app.
FLASK_APP=lvfs/__init__.py ./env/bin/flask initdb
FLASK_APP=lvfs/__init__.py ./env/bin/flask db stamp
FLASK_APP=lvfs/__init__.py ./env/bin/flask db upgrade

# Add the test GPG key to the keyring.
# ...