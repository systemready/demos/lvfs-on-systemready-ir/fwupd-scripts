#!/usr/bin/env bash

# Set virtual disk, QEMU binary, enter builder folder.
export VIRTDISK=$(realpath disk.img)
export QEMU_BIN=$(realpath ./qemu/build/qemu-system-aarch64)
cd uboot-builder/

# Start the VM with user networking enabled.
export QEMU_EXTRA='-nic user'
make qemu-fip