#!/usr/bin/expect -f
set timeout -1
spawn make qemu-fip

# Wait for UBOOT "stop autoboot".
expect "Hit any key to stop autoboot:"
send "a"

# Wait for uboot shell, install correct boot options.
expect "=>"
send "virtio scan; load virtio 0:1 \$fdt_addr qemu-arm64.dtb; load virtio 0:1 0x70000000 Image;\r"
expect "=>"
send "efidebug boot add -b 0000 ‘kernel’ virtio 0:1 Image -s 'ttyAMA0,115200 earlycon=pl011,mmio32,0x9000000 debug=7 default_hugepagesz=1024m hugepagesz=1024m hugepages=2 pci=pcie_bus_perf root=/dev/vda2 rw fstype=ext4 init=/sbin/init';\r"
expect "=>"
send "efidebug boot add -b 0001 ‘ESP’ virtio 0:1 EFI/UpdateCapsule;\r"
expect "=>"
send "efidebug boot order 0001 0000\r"
expect "=>"
send "boot\r"

# Wait for Ubuntu login, log in as root.
expect "ubuntu login:"
send "root\r"
expect "Password:"
send "root\r"

# Clone the fwupd repository into /fwupd.
puts "Reached login status."
expect "root@ubuntu:~#"
send "git clone https://github.com/fwupd/fwupd\r"
expect "root@ubuntu:~#"
send "cd fwupd\r"
expect "root@ubuntu:"
send "git checkout tags/1.7.1\r"
expect "root@ubuntu:"

# Entirely unsure why is this a requirement, apt.
send "cat /etc/apt/sources.list | sed -e 's/deb/deb-src/' >> /etc/apt/sources.list\r"
expect "root@ubuntu:"
send "apt clean\r"
expect "root@ubuntu:"
send "rm -rf /var/lib/apt/lists/*\r"
expect "root@ubuntu:"
send "mkdir /var/lib/apt/lists/partial\r"
expect "root@ubuntu:"
send "apt clean\r"
expect "root@ubuntu:"
send "apt update\r"
expect "root@ubuntu:"

# Install required apt packages for building, build.
send "apt --yes --force-yes install meson ninja-build gcc pkg-config cmake libglib2.0-dev libgudev-1.0-dev gobject-introspection gir1.2-xmlb-1.0 autoconf automake autopoint autotools-dev bubblewrap debhelper dh-autoreconf dh-strip-nondeterminism docbook docbook-to-man docbook-xml docbook-xsl dwz fontconfig fontconfig-config fonts-dejavu-core fonts-noto fonts-noto-core gcab gettext gir1.2-freedesktop gir1.2-gcab-1.0 gir1.2-gusb-1.0 gir1.2-jcat-1.0 gir1.2-json-1.0 gir1.2-mbim-1.0 gir1.2-modemmanager-1.0 gir1.2-pango-1.0 gir1.2-polkit-1.0 gir1.2-qmi-1.0 gir1.2-xmlb-1.0 gnu-efi gnutls-bin gtk-doc-tools intltool-debian libarchive-dev libarchive-zip-perl libassuan-dev libc6-dbg libcairo-gobject2 libcairo-script-interpreter2 libcairo2 libcairo2-dev libcroco3 libcurl4-gnutls-dev libdatrie1 libdebhelper-perl libefiboot-dev libefivar-dev libelf-dev libexpat1-dev libfile-stripnondeterminism-perl libfontconfig1 libfontconfig1-dev libfreetype-dev libfreetype6 libfreetype6-dev libgcab-dev libgcrypt20-dev libgirepository1.0-dev libglib2.0-doc libgmp-dev libgmpxx4ldbl libgnutls-dane0 libgnutls-openssl27 libgnutls28-dev libgnutlsxx28 libgpg-error-dev libgpgme-dev libgraphite2-3 libgusb-dev libharfbuzz0b libice-dev libice6 libidn2-dev libjcat-dev libjson-glib-dev libmbim-glib-dev libmbim-glib4 libmbim-proxy libmm-glib-dev libmm-glib0 libopts25 libosp5 libp11-kit-dev libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0 libpangoxft-1.0-0 libpci-dev libpixman-1-0 libpixman-1-dev libpng-dev libpolkit-gobject-1-dev libprotobuf-c-dev libprotobuf-c1 libprotobuf17 libprotoc17 libpthread-stubs0-dev libqmi-glib-dev libqmi-glib5 libqmi-proxy libsm-dev libsm6 libsqlite3-dev libsub-override-perl libsystemd-dev libtasn1-6-dev libthai-data libthai0 libtool libtool-bin libtss2-dev libudev-dev libumockdev0 libunbound8 libusb-1.0-0-dev libvala-0.48-0 libvalacodegen-0.48-0 libx11-dev libxau-dev libxcb-render0 libxcb-render0-dev libxcb-shm0 libxcb-shm0-dev libxcb1-dev libxdmcp-dev libxext-dev libxft2 libxmlb-dev libxrender-dev libxrender1 libxslt1.1 m4 mingw-w64-tools modemmanager-dev nettle-dev opensp po-debconf protobuf-c-compiler python3-cairo python3-gi-cairo python3-lxml python3-smartypants python3-toml python3-typogrify sgml-base sgml-data umockdev valac valac-0.48-vapi valac-bin valgrind x11-common x11proto-core-dev x11proto-dev x11proto-xext-dev xml-core xorg-sgml-doctools xsltproc xtrans-dev\r"
expect "root@ubuntu:"
send "meson build -Dplugin_msr=false -Dplugin_dell=false -Dbuild=standalone -Ddefault_library=static -Dman=false -Dtests=false -Dintrospection=false -Ddocs=none\r"
expect "root@ubuntu:"
send "ninja -C build\r"
expect "root@ubuntu:"
send "ninja -C build install\r"
expect "root@ubuntu:"
send "ldconfig\n"
expect "root@ubuntu:"

# Finished building! Shut down the machine.
send "shutdown now\r"
puts "Completed 'fwupd' build, exiting VM."
interact